from bs4 import BeautifulSoup
import requests
import spotipy
from spotipy.oauth2 import SpotifyOAuth
import pprint

#Spotify Authentication
CLIENT_ID ="YOUR CLIENT ID"
CLIENT_SECRET = "YOUR CLIENT SECRET "
REDIRECT_URL = "http://example.com"
scope = "playlist-modify-private"

sp = spotipy.Spotify(
    auth_manager=SpotifyOAuth(
        scope=scope,
        redirect_uri=REDIRECT_URL,
        client_id=CLIENT_ID,
        client_secret=CLIENT_SECRET,
        show_dialog=True,
        cache_path="token.txt"
    )
)
user_id = sp.current_user()["id"]

#Ask User for Date
entered_date = input("Enter a date (YYYY-MM-DD) :- ")

#Scrape billboard website for hot 100 list
URL = f"https://www.billboard.com/charts/hot-100/{entered_date}"
print(URL)

response = requests.get(URL)
response.raise_for_status()

website = response.text
soup = BeautifulSoup(website, 'html.parser')

songs_html = soup.find_all(name="span", class_="chart-element__information__song")
artists_html = soup.find_all(
    name="span", class_="chart-element__information__artist")

songs = [song.getText() for song in songs_html]
artists = [artist.getText() for artist in artists_html]

data = []
for i in range(len(songs)):
    dict = {
        "name": songs[i],
        "artist": artists[i]
    }
    data.append(dict)

#Search songs in spotify
song_uris = []
skipped = 0
for item in data:
    #result = sp.search(q=f"{item['name']} {item['artist']}", type="track")
    result = sp.search(q=f"{item['name']}", type="track")
    try:
        uri = result["tracks"]["items"][0]["uri"]
        song_uris.append(uri)
    except IndexError:
        skipped += 1
        print(f"{item['name']} doesn't exist in Spotify. Skipped.")

print(f"Skipped Songs : {skipped}")
print(f"Songs Found :{len(song_uris)}")

#Create Playlist
playlist = sp.user_playlist_create(user=user_id, name=f"{entered_date} Billboard 100", public=False)

#Add Songs to Playlist
sp.playlist_add_items(playlist_id=playlist["id"], items=song_uris)
